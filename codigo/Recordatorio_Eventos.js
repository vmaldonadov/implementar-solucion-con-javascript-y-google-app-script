function recordarEvento(){
  let calendario = CalendarApp.getDefaultCalendar(); // Obtener el calendario del usuario que ejecute el programa
  let fecha = new Date(); // Fecha actual
  let eventos = calendario.getEventsForDay(fecha); // Extraer todos los eventos del dia
  // Recorrer todos los eventos del dia
  eventos.forEach(function(evento){
    let fechaE = evento.getStartTime(); // Extraer fecha de recordatorio
    let fechaR = calcularFecha(fechaE);// Fecha en la que se enviara el recordatorio 1 hora antes
    let descripcion = evento.getDescription();
    if(!descripcion){
      descripcion = "ninguna";
    }
    let titulo = evento.getTitle();
    if(fechaR){
      let idH = "1WVJ9yB_nF_z6bmROr7U8EwR8uZD0jSckjIHcR3JBDcs"; // Obtener el ID de la hoja de calculo
      let hoja = SpreadsheetApp.openById(idH).getSheets()[0];

      let rango = hoja.getDataRange(); // Obtener el rango de datos de la Hoja de calculo
      let datos = rango.getValues(); // Obtener los datos de la Hoja de calculo
      // Recorrer todos lo datos
      for(let i = 1; i < datos.length; i++){
        // Obtener datos fila por fila
        let fila = datos[i];
        let nombre = fila[1];
        let correo = fila[2];
        // Envio del gmail
        enviarGmail(correo, "Recordatorio de evento: " + titulo,
        "Hola " + nombre + " tienes un evento pronto -- Descripción: " + descripcion);
      }
    }
  });
}

// Envio de correos para el recordatorio
function enviarGmail(destinatario,asunto,mensaje) {
  MailApp.sendEmail(destinatario,asunto,mensaje);
}

function calcularFecha(fechaE){
  let tiempoRestante = fechaE.getTime() - new Date().getTime(); // Tiempo que sobra desde el evento hasta el tiempo actual
  // Si el tiempo restante es mayor a una hora
  if(tiempoRestante >= (1*60*60*1000)){
    return new Date(fechaE.getTime() - (1*60*60*1000)); // fecha del evento menos una hora
  } else { // Caso contrario retorna null
    return null;
  }
}

